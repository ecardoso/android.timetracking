package com.ciandt.tt.service;

/**
 * Created by edgardcardoso on 26/08/14.
 */
public class TimeTrackingException extends  Exception {
    public TimeTrackingException() {
    }

    public TimeTrackingException(String detailMessage) {
        super(detailMessage);
    }

    public TimeTrackingException(String detailMessage, Throwable throwable) {
        super(detailMessage, throwable);
    }

    public TimeTrackingException(Throwable throwable) {
        super(throwable);
    }
}
