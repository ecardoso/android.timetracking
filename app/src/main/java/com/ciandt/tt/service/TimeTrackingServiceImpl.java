package com.ciandt.tt.service;

import android.text.TextUtils;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by edgardcardoso on 26/08/14.
 */
public class TimeTrackingServiceImpl implements TimeTrackingService {

    private String user;
    private String password;

    private final String USER_AGENT = "Android";

    @Override
    public TimeTrackingService user(String value) {
        this.user = value;
        return this;
    }

    @Override
    public TimeTrackingService password(String value) {
        this.password = value;
        return this;
    }

    @Override
    public void check(TimeTrackingListener listener) {
        TimeTrackingAsynckTask task = new TimeTrackingAsynckTask(user, password, listener);
        task.execute();
    }



}
