package com.ciandt.tt.service;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;


import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by edgardcardoso on 27/08/14.
 */
public class TimeTrackingAsynckTask extends AsyncTask<Void, Void, TimeTrackingAsynckTask.Response> {


    private String user;
    private String password;
    private TimeTrackingListener listener;

    private TimeTrackingException trackingException;

    private static final int CHECK_RECORDED = 1;
    private static final int INVALID_CREDENTIALS=2;
    private static final int OFFLINE_BACKEND = 3;

    public TimeTrackingAsynckTask(String user, String password, TimeTrackingListener listener) {
        this.user = user;
        this.password = password;
        this.listener = listener;
    }




    @Override
    protected Response doInBackground(Void[] params) {

        try {
            HttpRequest httpRequest = new HttpRequest();
            Response response = new Response();
            response.calendar = httpRequest.getTime();
            String json = httpRequest.checkInOut(user, password);
            response.callback = new Gson().fromJson(json, CallBack.class);
            return response;
        } catch (Exception e) {
            trackingException = new TimeTrackingException(e.getMessage());
            //Log.e("Error", e.getMessage());
            return null;
        }

    }

    @Override
    protected void onPostExecute(Response response) {
        if (trackingException != null) {
            listener.onError("Internal error:" + trackingException.getMessage(), response.calendar);
        }else if (!response.callback.isSuccess()) {
            listener.onError(response.callback.getErrorDetail(), response.calendar);
        } else {
            if (response.callback.getMsg().getType() == CHECK_RECORDED) {
                listener.onSucess(response.callback.getMsg().getMsg(), response.calendar);
            }
            else if (response.callback.getMsg().getType() == INVALID_CREDENTIALS) {
                listener.onError("Server Error: Invalid User/Password", response.calendar);
            }
            else if (response.callback.getMsg().getType() == OFFLINE_BACKEND) {
                listener.onError("Server Error: Backend out of service", response.calendar);
            }
            else{
                listener.onError("Server Error: Unknow", response.calendar);
            }
        }
    }

    class Response{
        CallBack callback;
        Calendar calendar;
    }
}
