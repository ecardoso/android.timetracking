package com.ciandt.tt.view;

import com.ciandt.tt.R;
import com.ciandt.tt.helpers.SharedPreferencesHelper;
import com.ciandt.tt.service.TimeTrackingListener;
import com.ciandt.tt.service.TimeTrackingService;
import com.ciandt.tt.service.TimeTrackingServiceImpl;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.Calendar;


public class MainActivity extends Activity {

    private Button buttonCheck;
    private EditText editTextUser;
    private EditText editTextPassword;
    private CheckBox checkBox;
    private TextView textviewLastcheck;


    private final String SHARED_PREFERENCES_USER = "user";
    private final String SHARED_PREFERENCES_PASSWORD = "password";
    private final String SHARED_PREFERENCES_CHECKED = "checked";
    private final String SHARED_PREFERENCES_TIME_CHECK = "time";
    private final String SHARED_PREFERENCES_FILE = "com.ciandt.tt";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        editTextUser = (EditText) findViewById(R.id.edittext_user);
        editTextPassword = (EditText) findViewById(R.id.edittext_password);
        buttonCheck = (Button) findViewById(R.id.button_check);
        checkBox = (CheckBox) findViewById(R.id.checkbox);
        textviewLastcheck = (TextView) findViewById(R.id.textview_lastcheck);

        handlerButtonEvent();
        handlerCheckboxEvent();

        readFromPreferences();

    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo ni = cm.getActiveNetworkInfo();
        if (ni == null) {
            // There are no active networks.
            return false;
        } else
            return true;
    }

    private void handlerCheckboxEvent(){
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

            }
        });
    }

    private void readFromPreferences(){
        boolean checked = SharedPreferencesHelper.readBoolean(this,SHARED_PREFERENCES_FILE,SHARED_PREFERENCES_CHECKED, false);
        if (checked){
            editTextUser.setText(SharedPreferencesHelper.read(this,SHARED_PREFERENCES_FILE, SHARED_PREFERENCES_USER,""));
            editTextPassword.setText(SharedPreferencesHelper.read(this, SHARED_PREFERENCES_FILE, SHARED_PREFERENCES_PASSWORD, ""));
            checkBox.setChecked(true);
        }
        textviewLastcheck.setText(SharedPreferencesHelper.read(this, SHARED_PREFERENCES_FILE, SHARED_PREFERENCES_TIME_CHECK, ""));
    }

    private void saveToPreferences(){

        if (checkBox.isChecked()){
            SharedPreferencesHelper.write(this,SHARED_PREFERENCES_FILE,SHARED_PREFERENCES_USER, editTextUser.getText().toString());
            SharedPreferencesHelper.write(this,SHARED_PREFERENCES_FILE,SHARED_PREFERENCES_PASSWORD, editTextPassword.getText().toString());
            SharedPreferencesHelper.write(this,SHARED_PREFERENCES_FILE,SHARED_PREFERENCES_CHECKED, true);
        }else{
            SharedPreferencesHelper.write(this,SHARED_PREFERENCES_FILE,SHARED_PREFERENCES_CHECKED, false);
        }
    }

    private void handlerButtonEvent(){

        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isNetworkConnected()){
                    showDialog("No Internet Connection. Try again later", false);
                    return;
                }

                saveToPreferences();


                buttonCheck.setEnabled(false);

                final ProgressDialog progressDialog = ProgressDialog.show(MainActivity.this, "Please wait...", "Check in/out in progress...", true);
                progressDialog.setCancelable(false);


                TimeTrackingService service = new TimeTrackingServiceImpl();
                service.user(editTextUser.getText().toString())
                        .password(editTextPassword.getText().toString())
                        .check(new TimeTrackingListener() {
                            @Override
                            public void onSucess(String msg, Calendar calendar) {
                                buttonCheck.setEnabled(true);
                                progressDialog.dismiss();

                                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
                                String lastCheck = sdf.format(calendar.getTime());
                                SharedPreferencesHelper.write(MainActivity.this, SHARED_PREFERENCES_FILE, SHARED_PREFERENCES_TIME_CHECK, lastCheck);
                                textviewLastcheck.setText(lastCheck);

                                showDialog(msg, true);
                            }

                            @Override
                            public void onError(String msg, Calendar calendar) {
                                buttonCheck.setEnabled(true);
                                progressDialog.dismiss();
                                showDialog(msg, false);
                            }
                        });
            }
        });
    }

    private void showDialog(String msg, boolean sucess){
        int icon;

        if (sucess){
            icon = R.drawable.ic_action_about;
        }else{
            icon = R.drawable.ic_alerts_and_states_error;
        }

        new AlertDialog.Builder(this)
                .setTitle("Time Tracking")
                .setMessage(msg)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .setIcon(icon)
                .show();
    }




}
